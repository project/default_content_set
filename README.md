CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Functionality
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides a number of enhancements to the Default Content for D8
module.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/default_content_set

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/default_content_set


FEATURES
--------

 * Configure one or multiple sets of content to export
 * Configure to which module the content should be exported
 * Fine-grained control over which content to export
 * A processing system which allows manipulating the exported content, for
   example to add path aliases to exported content
 * Configurable naming patterns for the exported files
 * Copies files that are being exported to the export module and back


FUNCTIONALITY
-------------

The module allows configuring any number of Default Content Sets which
determine which content is exported, how it is exported, and to where it is
exported. The export itself is done via a default-content-set:export Drush
command. The export is performed in a way that is picked up by Default Content
so that this module is not required for the import of the content, but for the
import of files.

The selection and the processing of the content to export is handled via
plugins so that it can be altered, amended and tailored to custom use-cases.


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

 * Default Content for D8 - https://www.drupal.org/project/default_content


INSTALLATION
------------

 * Install the Default Content Set module as you would normally install a
   contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Web services > Default
       content sets > Add set.


MAINTAINERS
-----------

 * Jan Stöckler (jan.stoeckler) - https://www.drupal.org/u/janstoeckler
 * Tobias Zimmermann (tstoeckler) - https://www.drupal.org/u/tstoeckler

Supporting organizations:

 * bio.logis Genetic Information Management GmbH -
   https://www.drupal.org/biologis-genetic-information-management-gmbh
