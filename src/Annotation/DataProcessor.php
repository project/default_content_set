<?php

namespace Drupal\default_content_set\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides an annotation for data provider plugins.
 *
 * @Annotation
 */
class DataProcessor extends Plugin {

  /**
   * The plugin ID of the data processor.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the data processor.
   *
   * @var string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public $label;

}
