<?php

namespace Drupal\default_content_set\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides an annotation for entity collection plugins.
 *
 * @Annotation
 */
class EntityCollection extends Plugin {

  /**
   * The plugin ID of the entity collection.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the entity collection.
   *
   * @var string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public $label;

}
