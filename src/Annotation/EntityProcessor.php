<?php

namespace Drupal\default_content_set\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides an annotation for entity provider plugins.
 *
 * @Annotation
 */
class EntityProcessor extends Plugin {

  /**
   * The plugin ID of the entity processor.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the entity processor.
   *
   * @var string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public $label;

}
