<?php

namespace Drupal\default_content_set\Commands;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Utility\Token;
use Drupal\default_content\Event\DefaultContentEvents;
use Drupal\default_content\ExporterInterface;
use Drupal\default_content_set\DefaultContent\DefaultContentSetExportEvent;
use Drupal\default_content_set\Entity\DefaultContentSetInterface;
use Drupal\hal\LinkManager\LinkManagerInterface;
use Drush\Commands\DrushCommands as OriginalDrushCommands;
use Symfony\Component\Console\Style\StyleInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\Serializer;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;

/**
 * Exports default content sets.
 */
class DrushCommands extends OriginalDrushCommands {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The transliterator.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * The link manager.
   *
   * @var \Drupal\hal\LinkManager\LinkManagerInterface
   */
  protected $linkManager;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The default content exporter.
   *
   * @var \Drupal\default_content\ExporterInterface
   */
  protected $exporter;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The relation domain URI for entity links.
   *
   * @var string
   */
  protected $linkDomain;

  /**
   * Constructs an export command.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Component\Transliteration\TransliterationInterface $transliteration
   *   The transliterator.
   * @param \Drupal\hal\LinkManager\LinkManagerInterface $link_manager
   *   The link manager.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   The serializer.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\default_content\ExporterInterface $exporter
   *   The default content exporter.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation.
   * @param string $link_domain
   *   The relation domain URI for entity links.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Token $token, TransliterationInterface $transliteration, LinkManagerInterface $link_manager, Serializer $serializer, EventDispatcherInterface $event_dispatcher, ExporterInterface $exporter, ModuleHandlerInterface $module_handler, TranslationInterface $translation, string $link_domain) {
    $this->entityTypeManager = $entity_type_manager;
    $this->token = $token;
    $this->transliteration = $transliteration;
    $this->serializer = $serializer;
    $this->linkManager = $link_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->exporter = $exporter;
    $this->moduleHandler = $module_handler;
    $this->setStringTranslation($translation);
    $this->linkDomain = $link_domain;

    parent::__construct();
  }

  /**
   * List default content sets
   *
   * @command default-content-set:list
   *
   * @aliases dcsl
   *
   * @usage default-content-set:list
   *   Usage description
   *
   * @field-labels
   *   label: Default content set
   *   id: Machine name
   *   module: Provided by module
   *   collections: Configured entity collections
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   */
  public function list($options = ['format' => 'table']) {
    $storage = $this->entityTypeManager->getStorage('default_content_set');
    $sets = $storage->loadMultiple();

    $this->io()->newLine();

    $rows = [];
    /** @var \Drupal\default_content_set\Entity\DefaultContentSetInterface $set */
    foreach ($sets as $set) {
      $entity_collections = [];
      foreach ($set->getEntityCollections() as $entityCollection) {
        $entity_collections[] = $entityCollection->getLabel();
      }
      $rows[$set->id()] = [
        'label' => $set->label(),
        'id' => $set->id(),
        'module' => $set->getModuleName(),
        'collections' => implode(', ', $entity_collections),
      ];
    }
    $result = new RowsOfFields($rows);
    return $result;
  }

  /**
   * Command description here.
   *
   * @command default-content-set:export
   *
   * @aliases dcse
   *
   * @param string $set Default content set.
   *
   * @usage default-content-set:export [set]
   *   Usage description
   */
  public function export(string $set) {
    $storage = $this->entityTypeManager->getStorage('default_content_set');
    /** @var \Drupal\default_content_set\Entity\DefaultContentSetInterface $set_entity */
    $set_entity = $storage->load($set);

    if (!$set_entity) {
      $this->io()->error(sprintf('The set %s does not exist.', $set));
      return 1;
    }

    $serialized_content = [];
    foreach ($this->getEntities($set_entity) as $entity) {
      $entity_type_id = $entity->getEntityTypeId();
      $unique_name = $this->getUniqueEntityName($serialized_content, $set_entity, $entity);

      $serialized_content += [$entity_type_id => []];
      $serialized_content[$entity_type_id][$unique_name] = $this->exportContent($set_entity, $entity);
    }

    $this->writeContent($set_entity, $serialized_content, $this->io());
  }

  /**
   * Returns a list of entities to export.
   *
   * @param \Drupal\default_content_set\Entity\DefaultContentSetInterface $default_content_set
   *   The default content set to get the entities for.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   A list of entities to export. These may be entities of different entity
   *   types.
   */
  protected function getEntities(DefaultContentSetInterface $default_content_set) {
    $entities = [];

    foreach ($default_content_set->getEntityCollections() as $entity_set) {
      $storage = $this->entityTypeManager->getStorage($entity_set->getEntityType()->id());
      $query = $storage->getQuery();
      $entity_set->applyConstraints($query);
      $ids = $query->execute();
      // @todo Implement batching of the loaded entities.
      $entities = array_merge($entities, $storage->loadMultiple($ids));
    }

    $processors = $default_content_set->getEntityProcessors();
    foreach ($entities as $entity) {
      foreach ($processors as $processor) {
        $processor->processEntity($entity);
      }
    }

    return $entities;
  }

  /**
   * Gets a name for the entity.
   *
   * @param \Drupal\default_content_set\Entity\DefaultContentSetInterface $default_content_set
   *   The default content set that this entity is part of.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the name for.
   *
   * @return string
   *   The name of the entity.
   */
  protected function getEntityName(DefaultContentSetInterface $default_content_set, ContentEntityInterface $entity) {
    $entity_type_id = $entity->getEntityTypeId();
    $pattern = $default_content_set->getNamePattern($entity_type_id);
    return $this->token->replace($pattern, [$entity_type_id => $entity]);
  }

  /**
   * Gets a unique name for each entity.
   *
   * @param string[][] $serialized_content
   *   The existing set of serialized content. As the content is keyed by
   *   entity
   *   type ID and unique name, this is checked to make sure that no duplicate
   *   name is returned.
   * @param \Drupal\default_content_set\Entity\DefaultContentSetInterface $default_content_set
   *   The default content set that this entity is part of.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to return the unique name for.
   *
   * @return string
   *   A unique name for the entity. This is generated by transliterating and
   *   lowercasing the entity label. To make the name unique an integer may be
   *   appended to the name.
   */
  protected function getUniqueEntityName(array $serialized_content, DefaultContentSetInterface $default_content_set, ContentEntityInterface $entity) {
    $name = $this->getEntityName($default_content_set, $entity);
    /* @see \Drupal\system\MachineNameController::transliterate() */
    $name = $this->transliteration->transliterate($name, $entity->language()->getId(), '-');
    $name = Unicode::strtolower($name);
    $name = preg_replace('/[^a-z0-9-]+/', '-', $name);

    if (!$name) {
      $name = '1';
    }

    $unique_name = $name;
    $suffix = 1;
    while (isset($serialized_content[$entity->getEntityTypeId()][$unique_name])) {
      ++$suffix;
      $unique_name = $name . $suffix;
    }
    return $unique_name;
  }

  /**
   * Exports an entity into a serialized string.
   *
   * This does not use \Drupal\default_content\Exporter::exportContent()
   * directly, as we process the normalized output before encoding it to
   * remove the IDs.
   *
   * @param \Drupal\default_content_set\Entity\DefaultContentSetInterface $set
   *   The default content set this entity belongs to.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to export.
   *
   * @return string
   *   The serialized entity.
   *
   * @see \Drupal\default_content\Exporter::exportContent()
   */
  protected function exportContent(DefaultContentSetInterface $set, ContentEntityInterface $entity) {
    $this->linkManager->setLinkDomain($this->linkDomain);

    $format = 'hal_json';
    $context = ['json_encode_options' => JSON_PRETTY_PRINT];
    $normalized = $this->serializer->normalize($entity, $format, $context);
    foreach ($set->getDataProcessors() as $processor) {
      $normalized = $processor->processData($normalized, $entity);
    }

    $serialized = $this->serializer->encode($normalized, $format, $context);
    // Add a newline to the end of the output to avoid notices about missing
    // newlines at the end of files.
    $serialized .= PHP_EOL;

    // Reset link domain.
    $this->linkManager->setLinkDomain(FALSE);
    $this->eventDispatcher->dispatch(DefaultContentEvents::EXPORT, new DefaultContentSetExportEvent($set, $entity));

    return $serialized;
  }

  /**
   * Writes the serialized content to the file system.
   *
   * @param \Drupal\default_content_set\Entity\DefaultContentSetInterface $set
   *   The default content set the content belongs to.
   * @param string[][] $serialized_content
   *   The serialized entities keyed by entity type ID and unique ID.
   * @param \Symfony\Component\Console\Style\StyleInterface $io
   *   The output handler.
   */
  protected function writeContent(DefaultContentSetInterface $set, array $serialized_content, StyleInterface $io) {
    if ($serialized_content) {
      $modules = system_rebuild_module_data();
      $directory = $modules[$set->getModuleName()]->getPath() . '/content';
      $this->exporter->writeDefaultContent($serialized_content, $directory);

      $message = (string) $this->t('Exported:');
      foreach ($serialized_content as $entity_type_id => $entities) {
        $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
        $message .= PHP_EOL . '- ' . (string) $entity_type->getCountLabel(count($entities));
      }
      $io->success($message);
    }
    else {
      $io->warning((string) $this->t('No content to export found.'));
    }
  }

}
