<?php

namespace Drupal\default_content_set\Controller;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\default_content_set\Entity\DefaultContentSetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Provides a controller for entity collection routes.
 */
class EntityCollectionController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity collection manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $entityCollectionManager;

  /**
   * Constructs an entity collection controller.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $entity_collection_manager
   *   The entity collection manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(PluginManagerInterface $entity_collection_manager, TranslationInterface $string_translation) {
    $this->entityCollectionManager = $entity_collection_manager;
    $this->setStringTranslation($string_translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.default_content_set.entity_collection'),
      $container->get('string_translation')
    );
  }

  /**
   * Builds a page for selecting which type of entity colleciton to add.
   */
  public function addPage(DefaultContentSetInterface $default_content_set) {
    $collection_definitions = $this->entityCollectionManager->getDefinitions();
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $route_name = 'entity.default_content_set.entity_collection_add_form';
    // Redirect if there's only one bundle available.
    if (count($collection_definitions) == 1) {
      $collection_plugin_ids = array_keys($collection_definitions);
      $collection_plugin_id = reset($collection_plugin_ids);

      $route_parameters = [
        'default_content_set' => $default_content_set->id(),
        'entity_collection_plugin' => $collection_plugin_id,
      ];
      $redirect_url = Url::fromRoute($route_name, $route_parameters, ['absolute' => TRUE]);
      return new RedirectResponse($redirect_url);
    }

    // Prepare the #bundles array for the template.
    foreach ($collection_definitions as $collection_plugin_id => $collection_definition) {
      $route_parameters = [
        'default_content_set' => $default_content_set->id(),
        'entity_collection_plugin' => $collection_plugin_id,
      ];
      $link = Link::createFromRoute($collection_definition['label'], $route_name, $route_parameters);
      $build['#bundles'][$collection_plugin_id] = [
        'label' => $collection_definition['label'],
        'description' => '',
        'add_link' => $link,
      ];
    }

    return $build;
  }

}
