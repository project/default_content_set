<?php

namespace Drupal\default_content_set\DefaultContent;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\default_content\Event\ExportEvent;
use Drupal\default_content_set\Entity\DefaultContentSetInterface;

/**
 * Provides an export event for default content sets.
 */
class DefaultContentSetExportEvent extends ExportEvent {

  /**
   * The default content set that is being exported.
   *
   * @var \Drupal\default_content_set\Entity\DefaultContentSetInterface
   */
  protected $defaultContentSet;

  /**
   * Constructs a new export event.
   *
   * @param \Drupal\default_content_set\Entity\DefaultContentSetInterface $default_content_set
   *   The default content set that is being exported.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The exported content entity.
   */
  public function __construct(DefaultContentSetInterface $default_content_set, ContentEntityInterface $entity) {
    parent::__construct($entity);

    $this->defaultContentSet = $default_content_set;
  }

  /**
   * Get the default content set that is being exported.
   *
   * @return \Drupal\default_content_set\Entity\DefaultContentSetInterface
   *   The default content set.
   */
  public function getDefaultContentSet() {
    return $this->defaultContentSet;
  }

}
