<?php

namespace Drupal\default_content_set\DefaultContent;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\default_content\Event\DefaultContentEvents;
use Drupal\default_content\Event\ExportEvent;
use Drupal\default_content\Event\ImportEvent;
use Drupal\default_content_set\Entity\DefaultContentSetInterface;
use Drupal\file\FileInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Reacts to default content exporting and importing.
 */
class DefaultContentSubscriber implements EventSubscriberInterface {

  /**
   * The file system handler.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The default content set storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Constructs a default content subscriber.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(FileSystemInterface $file_system, EntityTypeManagerInterface $entity_type_manager) {
    $this->fileSystem = $file_system;
    $this->storage = $entity_type_manager->getStorage('default_content_set');
  }

  /**
   * Reacts to exporting of default content.
   *
   * @param \Drupal\default_content\Event\ExportEvent $event
   *   The export event.
   */
  public function onExport(ExportEvent $event) {
    if (!($event instanceof DefaultContentSetExportEvent)) {
      return;
    }

    $entity = $event->getExportedEntity();
    if (!($entity instanceof FileInterface)) {
      return;
    }

    $uri = $entity->getFileUri();
    if ($this->fileSystem->uriScheme($uri) !== 'public') {
      return;
    }

    $directory = $this->fileSystem->dirname($uri);
    $copy_directory = $this->getCopyDirectory($event->getDefaultContentSet(), $directory);
    file_prepare_directory($copy_directory, FILE_CREATE_DIRECTORY);
    file_unmanaged_copy($uri, $copy_directory . '/' . $this->fileSystem->basename($uri), FILE_EXISTS_REPLACE);
  }

  /**
   * Reacts to importing of default content.
   *
   * @param \Drupal\default_content\Event\ImportEvent $event
   *   The import event.
   */
  public function onImport(ImportEvent $event) {
    // Figure out which sets are being imported.
    $imported_sets = [];
    /* @var \Drupal\default_content_set\Entity\DefaultContentSetInterface $set */
    foreach ($this->storage->loadMultiple() as $set) {
      if ($set->getModuleName() === $event->getModule()) {
        $imported_sets[] = $set;
      }
    }

    foreach ($imported_sets as $imported_set) {
      foreach ($event->getImportedEntities() as $entity) {
        if ($entity instanceof FileInterface) {
          $uri = $entity->getFileUri();
          if ($this->fileSystem->uriScheme($uri) === 'public') {
            $directory = $this->fileSystem->dirname($uri);
            $copy_directory = $this->getCopyDirectory($imported_set, $directory);
            $copy_path = $copy_directory . '/' . $this->fileSystem->basename($uri);
            if (file_exists($copy_path)) {
              file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
              file_unmanaged_copy($copy_path, $uri, FILE_EXISTS_REPLACE);
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      DefaultContentEvents::EXPORT => 'onExport',
      DefaultContentEvents::IMPORT => 'onImport',
    ];
  }

  /**
   * Returns the copy directory for a directory in the public files directory.
   *
   * @param \Drupal\default_content_set\Entity\DefaultContentSetInterface $default_content_set
   *   The default content set.
   * @param string $directory
   *   The URI of the directory to fetch the copy directory for.
   *
   * @return string
   *   The copy directory.
   */
  protected function getCopyDirectory(DefaultContentSetInterface $default_content_set, string $directory) {
    $modules = system_rebuild_module_data();
    $copy_directory = $modules[$default_content_set->getModuleName()]->getPath();
    $copy_directory .= '/files';
    if ($sub_directory = file_uri_target($directory)) {
      $copy_directory .= '/' . $sub_directory;
    }
    return $copy_directory;
  }

}
