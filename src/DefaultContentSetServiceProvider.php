<?php

namespace Drupal\default_content_set;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\default_content_set\Normalizer\EntityReferenceRevisionItemNormalizer;
use Drupal\default_content_set\Normalizer\FileEntityNormalizer;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Provides dynamic services for the Default Content Set module.
 */
class DefaultContentSetServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');

    if (isset($modules['entity_reference_revisions'])) {
      $container->register('default_content_set.normalizer.entity_reference_revision_item.hal', EntityReferenceRevisionItemNormalizer::class)
        ->addArgument(new Reference('hal.link_manager'))
        ->addArgument(new Reference('serializer.entity_resolver'))
        ->addArgument(new Reference('entity_type.manager'))
        ->addTag('normalizer', ['priority' => 25]);
    }

    if (isset($modules['file'])) {
      $container->register('default_content_set.normalizer.file_entity.hal', FileEntityNormalizer::class)
        ->addArgument(new Reference('hal.link_manager'))
        ->addArgument(new Reference('entity.manager'))
        ->addArgument(new Reference('module_handler'))
        ->addTag('normalizer', ['priority' => 25]);
    }
  }

}
