<?php

namespace Drupal\default_content_set\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Plugin\DefaultLazyPluginCollection;
use Drupal\default_content_set\Plugin\default_content_set\EntityCollectionPluginCollection;

/**
 * Provides a default content set entity.
 *
 * @ConfigEntityType(
 *   id = "default_content_set",
 *   label = @Translation("Default content set"),
 *   label_collection = @Translation("Default content sets"),
 *   label_singular = @Translation("default content set"),
 *   label_plural = @Translation("default content sets"),
 *   label_count = @PluralTranslation(
 *     singular = "@count default content set",
 *     plural = "@count default content sets",
 *   ),
 *   config_prefix = "set",
 *   config_export = {
 *     "id",
 *     "label",
 *     "entity_collections",
 *     "entity_processors",
 *     "data_processors",
 *     "name_patterns",
 *     "module",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/default-content-sets",
 *     "add-form" = "/admin/config/services/default-content-sets/add",
 *     "edit-form" = "/admin/config/services/default-content-sets/manage/{default_content_set}",
 *     "entity-collection-add-page" = "/admin/config/services/default-content-sets/manage/{default_content_set}/collections/add",
 *     "entity-collection-add-form" = "/admin/config/services/default-content-sets/manage/{default_content_set}/collections/add/{entity_collection_plugin}",
 *     "entity-collection-edit-form" = "/admin/config/services/default-content-sets/manage/{default_content_set}/collections/manage/{entity_collection}",
 *     "entity-collection-delete-form" = "/admin/config/services/default-content-sets/manage/{default_content_set}/collections/manage/{entity_collection}/delete",
 *     "delete-form" = "/admin/config/services/default-content-sets/manage/{default_content_set}/delete",
 *   },
 *   handlers = {
 *     "list_builder" = "Drupal\default_content_set\Entity\DefaultContentSetListBuilder",
 *     "form" = {
 *       "add" = "Drupal\default_content_set\Form\DefaultContentSetAddForm",
 *       "add-entity-collection" = "Drupal\default_content_set\Form\EntityCollectionAddForm",
 *       "edit" = "Drupal\default_content_set\Form\DefaultContentSetEditForm",
 *       "edit-entity-collection" = "Drupal\default_content_set\Form\EntityCollectionEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *       "delete-entity-collection" = "Drupal\default_content_set\Form\EntityCollectionDeleteForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *       "default_content_set" = "Drupal\default_content_set\Routing\EntityCollectionRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer default content sets",
 * )
 */
class DefaultContentSet extends ConfigEntityBase implements DefaultContentSetInterface {

  /**
   * The ID of the default content set.
   *
   * @var string
   */
  protected $id;

  /**
   * The label of the default content set.
   *
   * @var string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected $label;

  /**
   * The list of entity collection configurations for this default content set.
   *
   * @var array[]
   */
  protected $entity_collections = [];

  /**
   * The list of entity processor configurations for this default content set.
   *
   * @var array[]
   */
  protected $entity_processors = [];

  /**
   * The list of data processor configurations for this default content set.
   *
   * @var array[]
   */
  protected $data_processors = [];

  /**
   * A list of name patterns for this default content set.
   *
   * @var string[]
   */
  protected $name_patterns = [];

  /**
   * The module name of this default content set.
   *
   * @var string
   */
  protected $module;

  /**
   * The plugin collection for the entity sets.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection
   */
  protected $entityCollectionCollection;

  /**
   * The plugin collection for the entity processors.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection
   */
  protected $entityProcessorCollection;

  /**
   * The plugin collection for the data processors.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection
   */
  protected $dataProcessorCollection;

  /**
   * {@inheritdoc}
   */
  public function getEntityCollections() {
    if (!isset($this->entityCollectionCollection)) {
      $entity_collection_manager = \Drupal::service('plugin.manager.default_content_set.entity_collection');
      $this->entityCollectionCollection = new EntityCollectionPluginCollection($entity_collection_manager, $this->entity_collections);
      $this->entityCollectionCollection->sort();
    }
    return $this->entityCollectionCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityProcessors() {
    if (!isset($this->entityProcessorCollection)) {
      $entity_set_manager = \Drupal::service('plugin.manager.default_content_set.entity_processor');
      $this->entityProcessorCollection = new DefaultLazyPluginCollection($entity_set_manager, $this->entity_processors);
      $this->entityProcessorCollection->sort();
    }
    return $this->entityProcessorCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataProcessors() {
    if (!isset($this->dataProcessorCollection)) {
      $entity_set_manager = \Drupal::service('plugin.manager.default_content_set.data_processor');
      $this->dataProcessorCollection = new DefaultLazyPluginCollection($entity_set_manager, $this->data_processors);
      $this->dataProcessorCollection->sort();
    }
    return $this->dataProcessorCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getNamePattern(string $entity_type_id) {
    if (isset($this->name_patterns[$entity_type_id])) {
      return $this->name_patterns[$entity_type_id];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleName() {
    return $this->module;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'entity_collections' => $this->getEntityCollections(),
      'entity_processors' => $this->getEntityProcessors(),
      'data_processors' => $this->getDataProcessors(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $parameters = parent::urlRouteParameters($rel);
    if ($rel === 'entity-collection-add-form') {
      /* @var \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager */
      $plugin_manager = \Drupal::service('plugin.manager.default_content_set.entity_collection');
      $plugin_ids = array_keys($plugin_manager->getDefinitions());
      $parameters['entity_collection_plugin'] = reset($plugin_ids);
    }
    elseif (in_array($rel, ['entity-collection-edit-form', 'entity-collection-delete-form'], TRUE)) {
      $collection_ids = $this->getEntityCollections()->getInstanceIds();
      $parameters['entity_collection'] = reset($collection_ids);
    }
    return $parameters;
  }

}
