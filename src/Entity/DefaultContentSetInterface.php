<?php

namespace Drupal\default_content_set\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Provides an interface for default content set entities.
 */
interface DefaultContentSetInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * Returns the list of entity collections that are part of this set.
   *
   * @return \Drupal\default_content_set\Plugin\default_content_set\EntityCollectionInterface[]|\Drupal\default_content_set\Plugin\default_content_set\EntityCollectionPluginCollection
   *   The list of entity collections.
   *
   * @see \Drupal\default_content_set\Plugin\default_content_set\entity_collection\EntitySetInterface
   *   The list of entity collections keyed by the respective ID.
   */
  public function getEntityCollections();

  /**
   * Returns the list of entity processors for this default content set.
   *
   * @return \Drupal\default_content_set\Plugin\default_content_set\EntityProcessorInterface[]|\Drupal\Core\Plugin\DefaultLazyPluginCollection
   *   A list of entity processors.
   */
  public function getEntityProcessors();

  /**
   * Returns the list of data processors for this default content set.
   *
   * @return \Drupal\default_content_set\Plugin\default_content_set\DataProcessorInterface[]|\Drupal\Core\Plugin\DefaultLazyPluginCollection
   *   A list of data processors.
   */
  public function getDataProcessors();

  /**
   * Gets the name pattern for a given entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID to get the name pattern for.
   *
   * @return string|null
   *   The name pattern for the given entity type ID.
   */
  public function getNamePattern(string $entity_type_id);

  /**
   * Gets the name of the module to export the default content to.
   *
   * @return string
   *   The module name of the default content set.
   */
  public function getModuleName();

}
