<?php

namespace Drupal\default_content_set\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to add default content sets.
 */
class DefaultContentSetAddForm extends EntityForm {

  /**
   * The entity collection manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $entityCollectionManager;

  /**
   * The entity processor manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $entityProcessorManager;

  /**
   * The data processor manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $dataProcessorManager;

  /**
   * Constructs a default content set form.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface $entity_collection_manager
   *   The entity collection manager.
   * @var \Drupal\Component\Plugin\PluginManagerInterface $entity_processor_manager
   *   The entity processor manager.
   * @var \Drupal\Component\Plugin\PluginManagerInterface $data_processor_manager
   *   The data processor manager.
   */
  public function __construct(PluginManagerInterface $entity_collection_manager, PluginManagerInterface $entity_processor_manager, PluginManagerInterface $data_processor_manager) {
    $this->entityCollectionManager = $entity_collection_manager;
    $this->entityProcessorManager = $entity_processor_manager;
    $this->dataProcessorManager = $data_processor_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.default_content_set.entity_collection'),
      $container->get('plugin.manager.default_content_set.entity_processor'),
      $container->get('plugin.manager.default_content_set.data_processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\default_content_set\Entity\DefaultContentSetInterface $set */
    $set = $this->getEntity();

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $set->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [get_class($set), 'load'],
      ],
      '#default_value' => $set->id(),
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $set = $this->getEntity();

    drupal_set_message($this->t('Default content set %label has been created.', [
      '%label' => $set->label(),
    ]));
    $form_state->setRedirectUrl($set->toUrl('edit-form'));

    return $status;
  }

}
