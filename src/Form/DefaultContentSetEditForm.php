<?php

namespace Drupal\default_content_set\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\DefaultLazyPluginCollection;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to edit default content sets.
 */
class DefaultContentSetEditForm extends EntityForm {

  /**
   * The entity collection manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $entityCollectionManager;

  /**
   * The entity processor manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $entityProcessorManager;

  /**
   * The data processor manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $dataProcessorManager;

  /**
   * Constructs a default content set form.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface $entity_collection_manager
   *   The entity collection manager.
   * @var \Drupal\Component\Plugin\PluginManagerInterface $entity_processor_manager
   *   The entity processor manager.
   * @var \Drupal\Component\Plugin\PluginManagerInterface $data_processor_manager
   *   The data processor manager.
   */
  public function __construct(PluginManagerInterface $entity_collection_manager, PluginManagerInterface $entity_processor_manager, PluginManagerInterface $data_processor_manager) {
    $this->entityCollectionManager = $entity_collection_manager;
    $this->entityProcessorManager = $entity_processor_manager;
    $this->dataProcessorManager = $data_processor_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.default_content_set.entity_collection'),
      $container->get('plugin.manager.default_content_set.entity_processor'),
      $container->get('plugin.manager.default_content_set.data_processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\default_content_set\Entity\DefaultContentSetInterface $set */
    $set = $this->getEntity();

    $form['#tree'] = TRUE;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $set->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [get_class($set), 'load'],
      ],
      '#default_value' => $set->id(),
      '#disabled' => TRUE,
    ];

    $form = $this->buildEntityCollectionForm($form, $form_state);
    $form = $this->buildProcessorForm(
      $form,
      $form_state,
      'entity_processors',
      $this->t('Entity processors'),
      $this->entityProcessorManager,
      $set->getEntityProcessors()
    );
    $form = $this->buildProcessorForm(
      $form,
      $form_state,
      'data_processors',
      $this->t('Data processors'),
      $this->dataProcessorManager,
      $set->getDataProcessors()
    );

    /* @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */
    $entity_types = [];
    foreach ($set->getEntityCollections() as $collection_id => $collection) {
      $entity_type = $collection->getEntityType();
      $entity_types[$entity_type->id()] = $entity_type;
    }
    foreach ($entity_types as $entity_type_id => $entity_type) {
      $default_value = $set->getNamePattern($entity_type_id);
      if ($default_value === NULL) {
        $field_name = $entity_type->hasKey('label') ? $entity_type->getKey('label') : $entity_type->getKey('id');
        $default_value = "[$entity_type_id:$field_name]";
      }

      $form['name_patterns'][$entity_type_id] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name pattern for %entities', ['%entities' => $entity_type->getPluralLabel()]),
        '#default_value' => $default_value,
        '#required' => TRUE,
      ];
    }

    $form['module'] = [
      '#type' => 'select',
      '#title' => $this->t('Module'),
      '#description' => $this->t('Select the module that the content will be exported to.'),
      '#options' => $this->getModuleOptions(),
      '#default_value' => $set->getModuleName(),
    ];

    return parent::form($form, $form_state);
  }

  /**
   * Builds the entity collection form.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The entity collection form.
   */
  protected function buildEntityCollectionForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\default_content_set\Entity\DefaultContentSetInterface $set */
    $set = $this->getEntity();

    $form['entity_collections_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Content collections'),
      '#open' => TRUE,
      '#collapsible' => FALSE,
    ];
    $form['entity_collections_container']['entity_collections'] = [
      '#type' => 'table',
      '#header' => [
        'label' => $this->t('Label'),
        'entity_type' => [
          'data' => $this->t('Entity type'),
          'class' => [RESPONSIVE_PRIORITY_MEDIUM],
        ],
        'operations' => $this->t('Operations'),
      ],
      '#empty' => $this->t('No content collections configured.'),
    ];

    $entity_types = [];
    foreach ($set->getEntityCollections() as $collection_id => $collection) {
      $entity_type = $collection->getEntityType();
      $form['entity_collections_container']['entity_collections'][$collection_id]['label'] = [
        '#plain_text' => $collection->getLabel(),
      ];
      $form['entity_collections_container']['entity_collections'][$collection_id]['entity_type'] = [
        '#plain_text' => $collection->getEntityType()->getLabel(),
      ];
      $form['entity_collections_container']['entity_collections'][$collection_id]['operations'] = [
        '#type' => 'dropbutton',
        '#links' => [
          'edit' => [
            'title' => $this->t('Edit'),
            'url' => Url::fromRoute('entity.default_content_set.entity_collection_edit_form', [
              'default_content_set' => $set->id(),
              'entity_collection' => $collection_id,
            ]),
          ],
          'delete' => [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('entity.default_content_set.entity_collection_delete_form', [
              'default_content_set' => $set->id(),
              'entity_collection' => $collection_id,
            ]),
          ],
        ],
      ];

      // Collect the set of entity types for use below.
      $entity_types[$entity_type->id()] = $entity_type;
    }
    $form['entity_collections_container']['entity_collection_add'] = [
      '#type' => 'link',
      '#title' => $this->t('Add a new content collection'),
      '#url' => $set->toUrl('entity-collection-add-page'),
    ];
    return $form;
  }

  /**
   * Builds the entity processor or data processor form.
   *
   * @param array $form
   *   The entire form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_key
   *   The key to place the processor form under.
   * @param string|\Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The label of this form section.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The plugin manager for entity processors or data processors.
   * @param \Drupal\Core\Plugin\DefaultLazyPluginCollection $collection
   *   The plugin collection of entity processors or data processors.
   *
   * @return array
   *   The processor form.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the processor plugin cannot be instantiated.
   */
  protected function buildProcessorForm(array $form, FormStateInterface $form_state, string $form_key, $label, PluginManagerInterface $plugin_manager, DefaultLazyPluginCollection $collection) {
    $form[$form_key] = [
      '#type' => 'details',
      '#title' => $label,
      '#open' => TRUE,
    ];
    foreach ($plugin_manager->getDefinitions() as $plugin_id => $plugin_definition) {
      // @todo This does not pick up any changes if the configuration form
      //   itself uses Ajax.
      if ($collection->has($plugin_id)) {
        $plugin = $collection->get($plugin_id);
      }
      else {
        $plugin = $plugin_manager->createInstance($plugin_id);
      }

      $form[$form_key][$plugin_id] = [
        '#type' => 'container',
      ];
      $form[$form_key][$plugin_id]['status'] = [
        '#type' => 'checkbox',
        '#title' => $plugin_definition['label'],
        '#default_value' => $collection->has($plugin_id),
      ];
      if ($plugin instanceof PluginFormInterface) {
        // @todo This does not work without JavaScript.
        // @todo If the page is reloaded with a checked checkbox, the
        //   configuration form will not be shown.
        $ajax_wrapper = str_replace('_', '-', $form_key) . '-' . $plugin_id . '-wrapper';
        $form[$form_key][$plugin_id]['status']['#ajax'] = [
          'wrapper' => $ajax_wrapper,
          'callback' => '::processorAjaxCallback',
        ];

        $form[$form_key][$plugin_id]['configuration'] = [
          '#type' => 'container',
          '#prefix' => '<div id="' . $ajax_wrapper . '">',
          '#suffix' => '</div>',
        ];
        if ($form_state->hasValue([
          $form_key,
          $plugin_id,
          'status',
        ])) {
          $entity_processor_enabled = (bool) $form_state->getValue([
            $form_key,
            $plugin_id,
            'status',
          ]);
        }
        else {
          $entity_processor_enabled = $collection->has($plugin_id);
        }
        if ($entity_processor_enabled) {
          $entity_processor_form = &$form[$form_key][$plugin_id]['configuration'];
          $entity_processor_form_state = SubFormState::createForSubform($entity_processor_form, $form, $form_state);
          $entity_processor_form = $plugin->buildConfigurationForm($entity_processor_form, $entity_processor_form_state);
        }
      }
    }
    return $form;
  }

  /**
   * Gets the options for the default content set directory.
   *
   * @return string[]
   *   The directory options.
   */
  protected function getModuleOptions() {
    $options = [];
    foreach (system_rebuild_module_data() as $module_name => $module) {
      if (substr($module->getPath(), 0, 5) !== 'core/') {
        $options[$module_name] = $module->info['name'];
      }
    }
    natcasesort($options);
    return $options;
  }

  /**
   * Form Ajax callback to return the processor configuration form.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The configuration form of the entity processor or data processor that
   *   triggered the Ajax submission.
   */
  public static function processorAjaxCallback(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = array_merge(array_slice($triggering_element['#parents'], 0, -1), ['configuration']);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    /* @var \Drupal\default_content_set\Entity\DefaultContentSetInterface $entity */
    parent::copyFormValuesToEntity($entity, $form, $form_state);

    $this->submitProcessorForm(
      $form,
      $form_state,
      'entity_processors',
      $this->entityProcessorManager,
      $entity->getEntityProcessors()
    );
    $this->submitProcessorForm(
      $form,
      $form_state,
      'data_processors',
      $this->dataProcessorManager,
      $entity->getDataProcessors()
    );
  }

  /**
   * Submits the entity processor or data processor form.
   *
   * @param array $form
   *   The entire form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_key
   *   The key of the form that is to be submitted.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The plugin manager for entity processors or data processors.
   * @param \Drupal\Core\Plugin\DefaultLazyPluginCollection $collection
   *   The plugin collection of entity processors or data processors.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the processor plugin cannot be instantiated.
   */
  protected function submitProcessorForm(array $form, FormStateInterface $form_state, string $form_key, PluginManagerInterface $plugin_manager, DefaultLazyPluginCollection $collection) {
    foreach (array_keys($plugin_manager->getDefinitions()) as $plugin_id) {
      if ($form_state->getValue([$form_key, $plugin_id, 'status'])) {
        $plugin = $plugin_manager->createInstance($plugin_id);
        if ($plugin instanceof PluginFormInterface) {
          $entity_processor_form = &$form[$form_key][$plugin_id]['configuration'];
          $entity_processor_form_state = SubFormState::createForSubform($entity_processor_form, $form, $form_state);
          $plugin->submitConfigurationForm($entity_processor_form, $entity_processor_form_state);
        }

        $collection->set($plugin_id, $plugin);
      }
      elseif ($collection->has($plugin_id)) {
        $collection->removeInstanceId($plugin_id);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->validateProcessorForm($form, $form_state, 'entity_processors', $this->entityProcessorManager);
    $this->validateProcessorForm($form, $form_state, 'data_processors', $this->dataProcessorManager);
  }

  /**
   * Validates the entity processor or data processor form.
   *
   * @param array $form
   *   The entire form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_key
   *   The key of the form that is to be submitted.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The plugin manager for entity processors or data processors.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the processor plugin cannot be instantiated.
   */
  protected function validateProcessorForm(array &$form, FormStateInterface $form_state, string $form_key, PluginManagerInterface $plugin_manager) {
    foreach (array_keys($plugin_manager->getDefinitions()) as $plugin_id) {
      if ($form_state->getValue([$form_key, $plugin_id, 'status'])) {
        $plugin = $plugin_manager->createInstance($plugin_id);
        if ($plugin instanceof PluginFormInterface) {
          $subform = &$form[$form_key][$plugin_id]['configuration'];
          $subform_state = SubFormState::createForSubform($subform, $form, $form_state);
          $plugin->validateConfigurationForm($subform, $subform_state);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $set = $this->getEntity();

    drupal_set_message($this->t('Default content set %label has been saved.', [
      '%label' => $set->label(),
    ]));
    $form_state->setRedirectUrl($set->toUrl('collection'));

    return $status;
  }

}
