<?php

namespace Drupal\default_content_set\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\default_content_set\Entity\DefaultContentSetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides an add form for entity collections.
 */
class EntityCollectionAddForm extends EntityCollectionFormBase implements ContainerInjectionInterface {

  /**
   * The entity collection manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $entityCollectionManager;

  /**
   * Constructs an entity collection add form.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $entity_collection_manager
   *   The entity collection manager.
   */
  public function __construct(PluginManagerInterface $entity_collection_manager) {
    $this->entityCollectionManager = $entity_collection_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('plugin.manager.default_content_set.entity_collection'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DefaultContentSetInterface $default_content_set = NULL, $entity_collection_plugin = NULL) {
    if (!$default_content_set) {
      throw new NotFoundHttpException();
    }
    $this->defaultContentSet = $default_content_set;

    if (!is_string($entity_collection_plugin) || !$this->entityCollectionManager->hasDefinition($entity_collection_plugin)) {
      throw new NotFoundHttpException();
    }
    $this->entityCollection = $this->entityCollectionManager->createInstance($entity_collection_plugin, [
      'plugin' => $entity_collection_plugin,
    ]);

    return parent::buildForm($form, $form_state, $default_content_set);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $collections = $this->defaultContentSet->getEntityCollections();
    $collections->set($form_state->getValue('id'), $this->entityCollection);

    parent::submitForm($form, $form_state);

    drupal_set_message($this->t('Entity collection %label has been added.', [
      '%label' => $this->entityCollection->getLabel(),
    ]));
  }

  /**
   * Returns the title for the entity collection add form.
   */
  public static function addTitle(DefaultContentSetInterface $default_content_set, $entity_collection_plugin) {
    $entity_collection_manager = \Drupal::service('plugin.manager.default_content_set.entity_collection');
    $plugin_definition = $entity_collection_manager->getDefinition($entity_collection_plugin);

    return t('Add %label content collection', [
      '%label' => $plugin_definition['label'],
    ]);
  }

}
