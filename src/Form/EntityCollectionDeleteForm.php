<?php

namespace Drupal\default_content_set\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\default_content_set\Entity\DefaultContentSetInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a delete form for entity collections.
 */
class EntityCollectionDeleteForm extends ConfirmFormBase {

  /**
   * The default content set that the entity collection is being deleted from.
   *
   * @var \Drupal\default_content_set\Entity\DefaultContentSetInterface
   */
  protected $defaultContentSet;

  /**
   * The entity collection that is being deleted.
   *
   * @var \Drupal\default_content_set\Plugin\default_content_set\EntityCollectionInterface
   */
  protected $entityCollection;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_content_set_entity_collection_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DefaultContentSetInterface $default_content_set = NULL, $entity_collection = NULL) {
    if (!$default_content_set) {
      throw new NotFoundHttpException();
    }
    $this->defaultContentSet = $default_content_set;

    $entity_collections = $default_content_set->getEntityCollections();
    if (!$entity_collection || !$entity_collections->has($entity_collection)) {
      throw new NotFoundHttpException();
    }
    $this->entityCollection = $entity_collections->get($entity_collection);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the content collection %label', [
      '%label' => $this->entityCollection->getLabel(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->defaultContentSet->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->defaultContentSet->getEntityCollections()->removeInstanceId($this->entityCollection->getId());
    $this->defaultContentSet->save();

    drupal_set_message($this->t('The content collection %label has been deleted.', [
      '%label' => $this->entityCollection->getLabel(),
    ]));
    $form_state->setRedirectUrl($this->defaultContentSet->toUrl('edit-form'));
  }

  /**
   * Returns the title of the delete form for entity collections.
   */
  public static function deleteTitle(DefaultContentSetInterface $default_content_set, $entity_collection) {
    $entity_collections = $default_content_set->getEntityCollections();
    if ($entity_collections->has($entity_collection)) {
      /* @var \Drupal\default_content_set\Plugin\default_content_set\EntityCollectionInterface $entity_collection_instance */
      $entity_collection_instance = $entity_collections->get($entity_collection);
      return t('Delete content collection %label', [
        '%label' => $entity_collection_instance->getLabel(),
      ]);
    }
  }

}
