<?php

namespace Drupal\default_content_set\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\default_content_set\Entity\DefaultContentSetInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides an edit form for entity collections.
 */
class EntityCollectionEditForm extends EntityCollectionFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DefaultContentSetInterface $default_content_set = NULL, $entity_collection = NULL) {
    if (!$default_content_set) {
      throw new NotFoundHttpException();
    }
    $this->defaultContentSet = $default_content_set;

    $entity_collections = $default_content_set->getEntityCollections();
    if (!$entity_collection || !$entity_collections->has($entity_collection)) {
      throw new NotFoundHttpException();
    }
    $this->entityCollection = $entity_collections->get($entity_collection);

    return parent::buildForm($form, $form_state, $default_content_set);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('id') !== $this->entityCollection->getId()) {
      $collections = $this->defaultContentSet->getEntityCollections();
      $collections->removeInstanceId($this->entityCollection->getId());
      $collections->set($form_state->getValue('id'), $this->entityCollection);
    }
    parent::submitForm($form, $form_state);

    drupal_set_message($this->t('Entity collection %label has been updated.', [
      '%label' => $this->entityCollection->getLabel(),
    ]));
  }

  /**
   *
   */
  public static function editTitle(DefaultContentSetInterface $default_content_set, $entity_collection) {
    $entity_collections = $default_content_set->getEntityCollections();

    /* @var \Drupal\default_content_set\Plugin\default_content_set\EntityCollectionInterface $entity_collection_instance */
    $entity_collection_instance = $entity_collections->get($entity_collection);
    return t('Edit content collection %label', [
      '%label' => $entity_collection_instance->getLabel(),
    ]);
  }

}
