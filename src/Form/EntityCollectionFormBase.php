<?php

namespace Drupal\default_content_set\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\default_content_set\Entity\DefaultContentSetInterface;

/**
 * Provides a base form for entity collections.
 */
abstract class EntityCollectionFormBase implements FormInterface {

  use StringTranslationTrait;

  /**
   * The default content set the collection that is being edited belongs to.
   *
   * @var \Drupal\default_content_set\Entity\DefaultContentSetInterface
   */
  protected $defaultContentSet;

  /**
   * The entity collection that is being edited.
   *
   * @var \Drupal\default_content_set\Plugin\default_content_set\EntityCollectionInterface
   */
  protected $entityCollection;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_content_set_entity_collection_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DefaultContentSetInterface $default_content_set = NULL) {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->entityCollection->getLabel(),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this->defaultContentSet->getEntityCollections(), 'has'],
      ],
      '#default_value' => $this->entityCollection->getId(),
    ];

    if ($this->entityCollection instanceof PluginFormInterface) {
      $form = $this->entityCollection->buildConfigurationForm($form, $form_state);
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['button']],
      '#url' => $this->defaultContentSet->toUrl('edit-form'),
      '#cache' => [
        'contexts' => [
          'url.query_args:destination',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->entityCollection instanceof PluginFormInterface) {
      $this->entityCollection->validateConfigurationForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $configuration = $this->entityCollection->getConfiguration();
    $configuration['id'] = $form_state->getValue('id');
    $configuration['label'] = $form_state->getValue('label');
    $this->entityCollection->setConfiguration($configuration);
    if ($this->entityCollection instanceof PluginFormInterface) {
      $this->entityCollection->submitConfigurationForm($form, $form_state);
    }

    $this->defaultContentSet->save();

    $form_state->setRedirectUrl($this->defaultContentSet->toUrl('edit-form'));
  }

}
