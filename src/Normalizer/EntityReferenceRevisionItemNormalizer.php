<?php

namespace Drupal\default_content_set\Normalizer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_reference_revisions\Normalizer\EntityReferenceRevisionItemNormalizer as BaseEntityReferenceRevisionItemNormalizer;
use Drupal\entity_reference_revisions\Plugin\Field\FieldType\EntityReferenceRevisionsItem;
use Drupal\hal\LinkManager\LinkManagerInterface;
use Drupal\serialization\EntityResolver\EntityResolverInterface;

/**
 * Defines a class for normalizing EntityReferenceRevisionItems.
 *
 * In contrast to the parent implementation, this accounts for missing target
 * revision IDs in the normalized output.
 */
class EntityReferenceRevisionItemNormalizer extends BaseEntityReferenceRevisionItemNormalizer {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = EntityReferenceRevisionsItem::class;

  /**
   * Constructs an EntityReferenceRevisionItemNormalizer object.
   *
   * @param \Drupal\hal\LinkManager\LinkManagerInterface $link_manager
   *   The hypermedia link manager.
   * @param \Drupal\serialization\EntityResolver\EntityResolverInterface $entity_Resolver
   *   The entity resolver.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(LinkManagerInterface $link_manager, EntityResolverInterface $entity_Resolver, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($link_manager, $entity_Resolver);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function constructValue($data, $context) {
    // Avoid an undefined index in the parent. This also allows to check
    // for a NULL revision ID below.
    if (!isset($data['target_revision_id'])) {
      $data['target_revision_id'] = NULL;
    }

    $value = parent::constructValue($data, $context);

    if ($value) {
      // If the target revision ID was not provided in the data, use the default
      // revision as the target revision ID.
      /** @var \Drupal\entity_reference_revisions\Plugin\Field\FieldType\EntityReferenceRevisionsItem $item */
      $item = $context['target_instance'];
      $target_type = $item->getFieldDefinition()->getSetting('target_type');
      /** @var \Drupal\Core\Entity\RevisionableInterface $target */
      $target = $this->entityTypeManager->getStorage($target_type)->load($value['target_id']);
      $value['target_revision_id'] = $target->getRevisionId();
    }

    return $value;
  }

}
