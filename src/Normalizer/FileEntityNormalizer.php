<?php

namespace Drupal\default_content_set\Normalizer;

use Drupal\file\FileInterface;
use Drupal\hal\Normalizer\ContentEntityNormalizer;

/**
 * Provides a normalizer for files.
 *
 * This effectively disables \Drupal\hal\Normalizer\FileEntityNormalizer.
 * Instead of downloading the file over HTTP as part of the denormalization we
 * deal with the files explicitly as part of the default content export and
 * import process.
 *
 * @see \Drupal\default_content_set\DefaultContent\DefaultContentSubscriber
 */
class FileEntityNormalizer extends ContentEntityNormalizer {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = FileInterface::class;

}
