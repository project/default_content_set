<?php

namespace Drupal\default_content_set\Plugin\default_content_set;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an interface for data processors.
 *
 * Dat processors modify the normalized entity output before it is written to
 * file when it is exported as part of a default content set.
 *
 * Data processors need to implement PluginInspectionInterface so they can be
 * sorted by their plugin ID.
 *
 * @see \Drupal\Core\Plugin\DefaultLazyPluginCollection::sortHelper()
 */
interface DataProcessorInterface {

  /**
   * Processes the normalized entity data.
   *
   * @param array $normalized
   *   The normalized entity data.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that has been normalized.
   *
   * @return array
   *   The processed, normalized entity output.
   */
  public function processData(array $normalized, EntityInterface $entity);

}
