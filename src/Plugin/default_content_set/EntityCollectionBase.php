<?php

namespace Drupal\default_content_set\Plugin\default_content_set;

use Drupal\Core\Plugin\PluginBase;

/**
 * Provides a base entity collection.
 */
abstract class EntityCollectionBase extends PluginBase implements EntityCollectionInterface {

  /**
   * Constructs a base entity collection.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->getConfiguration()['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->getConfiguration()['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'id' => '',
      'label' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

}
