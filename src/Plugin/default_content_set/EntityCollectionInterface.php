<?php

namespace Drupal\default_content_set\Plugin\default_content_set;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Provides an interface for entity sets.
 *
 * Entity sets describe a certain set of entities of a given type by holding a
 * list of constraints that can be applied to an entity query.
 *
 * Note the distinction between entity collections and default content sets:
 * default content sets contain any number of entity collections plus further
 * configuration.
 *
 * Entity collections are required to implement ConfigurablePluginInterface so
 * that we can generically configure the label and ID of the collections.
 *
 * @see \Drupal\default_content_set\Form\EntityCollectionFormBase
 */
interface EntityCollectionInterface extends ConfigurablePluginInterface {

  /**
   * Returns the ID of the entity collection.
   *
   * This is unique per default content set that the entity collection is
   * attached to.
   *
   * @return string
   *   The entity collection ID.
   */
  public function getId();

  /**
   * Returns the label of the entity collection.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The entity collection label.
   */
  public function getLabel();

  /**
   * Returns the entity type that entities of this collection belong to.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The entity type.
   */
  public function getEntityType();

  /**
   * Applies constraints to an entity query.
   *
   * Constraints can be conditions or a range, for example.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The entity query to apply constraints to.
   */
  public function applyConstraints(QueryInterface $query);

}
