<?php

namespace Drupal\default_content_set\Plugin\default_content_set;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\default_content_set\Annotation\EntityCollection;

/**
 * Provides a plugin manager for entity collections.
 */
class EntityCollectionManager extends DefaultPluginManager {

  /**
   * Constructs an entity collection manager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend to cache the plugin definitions.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/default_content_set/entity_collection',
      $namespaces,
      $module_handler,
      EntityCollectionInterface::class,
      EntityCollection::class
    );

    $this->alterInfo('default_content_set_entity_collection_info');
    $this->setCacheBackend($cache_backend, 'default_content_set_entity_collection_plugins');
  }

}
