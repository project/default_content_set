<?php

namespace Drupal\default_content_set\Plugin\default_content_set;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * Implements a plugin collection for entity collections.
 *
 * Because multiple entity collections with the same plugin ID (but different
 * configuration) can be part of the same default content set, the plugin ID
 * cannot be used as the ID of the configured plugin instance. Therefore a
 * dedicated ID can be configured as part of the entity collection that is
 * stored in the 'id' key, while the plugin ID is stored in the 'plugin' key.
 */
class EntityCollectionPluginCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   */
  protected $pluginKey = 'plugin';

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID) {
    /* @var \Drupal\default_content_set\Plugin\default_content_set\EntityCollectionInterface $a */
    $a = $this->get($aID);
    /* @var \Drupal\default_content_set\Plugin\default_content_set\EntityCollectionInterface $b */
    $b = $this->get($bID);
    return strnatcasecmp($a->getLabel(), $b->getLabel());
  }

}
