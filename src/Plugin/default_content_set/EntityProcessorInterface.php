<?php

namespace Drupal\default_content_set\Plugin\default_content_set;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an interface for entity processors.
 *
 * Entity processors modify an entity before it is normalized when it is
 * exported as part of a default content set.
 *
 * Entity processors need to implement PluginInspectionInterface so they can
 * be sorted by their plugin ID.
 *
 * @see \Drupal\Core\Plugin\DefaultLazyPluginCollection::sortHelper()
 */
interface EntityProcessorInterface extends PluginInspectionInterface {

  /**
   * Processes an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to process.
   */
  public function processEntity(EntityInterface $entity);

}
