<?php

namespace Drupal\default_content_set\Plugin\default_content_set;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\default_content_set\Annotation\EntityProcessor;

/**
 * Provides a plugin manager for entity processors.
 */
class EntityProcessorManager extends DefaultPluginManager {

  /**
   * Constructs an entity processor manager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend to cache the plugin definitions.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/default_content_set/entity_processor',
      $namespaces,
      $module_handler,
      EntityProcessorInterface::class,
      EntityProcessor::class
    );

    $this->alterInfo('default_content_set_entity_processor_info');
    $this->setCacheBackend($cache_backend, 'default_content_set_entity_processor_plugins');
  }

}
