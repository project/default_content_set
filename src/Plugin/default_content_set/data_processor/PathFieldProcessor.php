<?php

namespace Drupal\default_content_set\Plugin\default_content_set\data_processor;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Path\AliasStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\default_content_set\Plugin\default_content_set\DataProcessorBase;
use Drupal\path\Plugin\Field\FieldType\PathFieldItemList;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a data processor that adds an entity's path alias.
 *
 * @DataProcessor(
 *   id = "path",
 *   label = @Translation("Add path aliases"),
 * )
 */
class PathFieldProcessor extends DataProcessorBase implements ContainerFactoryPluginInterface {

  /**
   * The alias storage.
   *
   * @var \Drupal\Core\Path\AliasStorageInterface
   */
  protected $aliasStorage;

  /**
   * Constructs a path field processor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Path\AliasStorageInterface $alias_storage
   *   The path alias storage.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, AliasStorageInterface $alias_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->aliasStorage = $alias_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.alias_storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processData(array $normalized, EntityInterface $entity) {
    if (($entity instanceof FieldableEntityInterface) && $entity->hasLinkTemplate('canonical')) {
      $alias = $this->aliasStorage->lookupPathAlias('/' . $entity->toUrl()->getInternalPath(), $entity->language()->getId());
      if ($alias) {
        foreach ($entity->getFields() as $field) {
          if (!($field instanceof PathFieldItemList)) {
            continue;
          }

          // Mimic the field structure that would be returned if this field
          // contained any items.
          $normalized[$field->getName()] = [['alias' => $alias]];
        }
      }

    }
    return $normalized;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

}
