<?php

namespace Drupal\default_content_set\Plugin\default_content_set\data_processor;

use Drupal\Core\Entity\EntityInterface;
use Drupal\default_content_set\Plugin\default_content_set\DataProcessorBase;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides a data processor that removes any serial IDs from the output.
 *
 * @DataProcessor(
 *   id = "remove_id",
 *   label = @Translation("Remove serial IDs"),
 * )
 */
class RemoveIdProcessor extends DataProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function processData(array $normalized, EntityInterface $entity) {
    $entity_type = $entity->getEntityType();

    unset($normalized['_links']['self']);
    if (isset($normalized['_embedded'])) {
      foreach ($normalized['_embedded'] as $type => &$embedded_per_type) {
        foreach ($embedded_per_type as $index => &$embedded) {
          unset($embedded['_links']['self']);
        }
        unset($normalized['_links'][$type]);
      }
    }

    $id_key = $entity_type->getKey('id');
    unset($normalized[$id_key]);

    if ($entity_type->hasKey('revision')) {
      unset($normalized[$entity_type->getKey('revision')]);
    }

    if ($entity instanceof Paragraph) {
      unset($normalized['parent_id']);
    }

    return $normalized;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

}
