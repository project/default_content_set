<?php

namespace Drupal\default_content_set\Plugin\default_content_set\entity_collection;

use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\default_content_set\Plugin\default_content_set\EntityCollectionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base content entity collection implementation.
 *
 * Depending on the capabilities of the given entity type, this allows
 * configuring the following constraints:
 * - Limiting the collection by bundle
 * - Limiting the collection to published entities
 * - Limiting the collection to specific entities, identified by UUID
 *
 * @EntityCollection(
 *   id = "content_entity",
 *   deriver = "Drupal\default_content_set\Plugin\Deriver\ContentEntityCollectionDeriver",
 * )
 */
class ContentEntityCollection extends EntityCollectionBase implements PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle information.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Constructs an entity set.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle information.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityRepositoryInterface $entity_repository, TranslationInterface $string_translation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityRepository = $entity_repository;
    $this->setStringTranslation($string_translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity.repository'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityType() {
    return $this->entityTypeManager->getDefinition($this->getDerivativeId());
  }

  /**
   * {@inheritdoc}
   */
  public function applyConstraints(QueryInterface $query) {
    $configuration = $this->getConfiguration();
    foreach ($configuration['conditions'] as $condition) {
      $condition += [
        'value' => NULL,
        'operator' => NULL,
        'language' => NULL,
      ];
      $query->condition(
        $condition['field'],
        $condition['value'],
        $condition['operator'],
        $condition['langcode']
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'conditions' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return ['module' => [$this->getEntityType()->getProvider()]];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $entity_type = $this->getEntityType();
    $configuration = $this->getConfiguration();

    $form['conditions'] = [
      '#tree' => TRUE,
    ];

    if ($entity_type->hasKey('bundle')) {
      $bundle_key = $entity_type->getKey('bundle');
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type->id());
      $options = array_combine(array_keys($bundles), array_column($bundles, 'label'));
      $form['conditions'][$bundle_key] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Filter @entities by %bundle', [
          '@entities' => $entity_type->getPluralLabel(),
          '%bundle' => $entity_type->getBundleLabel(),
        ]),
        '#options' => $options,
        '#weight' => 0,
      ];
      if (isset($configuration['conditions'][$bundle_key])) {
        $form['conditions'][$bundle_key]['#default_value'] = $configuration['conditions'][$bundle_key]['value'];
      }
    }

    if ($entity_type->entityClassImplements(EntityPublishedInterface::class)) {
      $published_key = $entity_type->getKey('published');
      $form['conditions'][$published_key] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Export only published @entities', [
          '@entities' => $entity_type->getPluralLabel(),
        ]),
        '#weight' => 10,
      ];
      if (isset($configuration['conditions'][$published_key])) {
        $form['conditions'][$published_key]['#default_value'] = TRUE;
      }
    }

    if ($entity_type->hasKey('uuid')) {
      $uuid_key = $entity_type->getKey('uuid');
      $form['conditions'][$uuid_key] = [
        '#type' => 'entity_autocomplete',
        '#title' => $this->t('Export only specific @entities', [
          '@entities' => $entity_type->getPluralLabel(),
        ]),
        '#description' => $this->t('Separate multiple @entities with a comma.', [
          '@entities' => $entity_type->getPluralLabel(),
        ]),
        '#target_type' => $entity_type->id(),
        '#tags' => TRUE,
        '#weight' => 20,
      ];
      if (isset($configuration['conditions'][$uuid_key])) {
        $entities = [];
        foreach ($configuration['conditions'][$uuid_key]['value'] as $uuid) {
          $entities[] = $this->entityRepository->loadEntityByUuid($entity_type->id(), $uuid);
        }
        $form['conditions'][$uuid_key]['#default_value'] = $entities;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $entity_type = $this->getEntityType();
    if ($entity_type->hasKey('bundle')) {
      $bundle_key = $entity_type->getKey('bundle');
      $bundles = $form_state->getValue(['conditions', $bundle_key], []);
      $bundles = array_keys(array_filter($bundles));
      if ($bundles) {
        $configuration['conditions'][$bundle_key] = [
          'field' => $bundle_key,
          'value' => $bundles,
          'operator' => 'IN',
        ];
      }
      else {
        unset($configuration['conditions'][$bundle_key]);
      }
    }

    if ($entity_type->entityClassImplements(EntityPublishedInterface::class)) {
      $published_key = $entity_type->getKey('published');
      if ($form_state->getValue(['conditions', $published_key], FALSE)) {
        $configuration['conditions'][$published_key] = [
          'field' => $published_key,
          'value' => TRUE,
          'operator' => '=',
        ];
      }
      else {
        unset($configuration['conditions'][$published_key]);
      }
    }

    if ($entity_type->hasKey('uuid')) {
      $uuid_key = $entity_type->getKey('uuid');
      if ($values = $form_state->getValue(['conditions', $uuid_key])) {
        $ids = array_column($values, 'target_id');
        $uuids = [];

        $storage = $this->entityTypeManager->getStorage($entity_type->id());
        foreach ($storage->loadMultiple($ids) as $entity) {
          $uuids[] = $entity->uuid();
        }

        $configuration['conditions'][$uuid_key] = [
          'field' => $uuid_key,
          'value' => $uuids,
          'operator' => 'IN',
        ];
      }
      else {
        unset($configuration['conditions'][$uuid_key]);
      }
    }

    $this->setConfiguration($configuration);
  }

}
