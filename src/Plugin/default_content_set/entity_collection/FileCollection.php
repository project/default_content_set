<?php

namespace Drupal\default_content_set\Plugin\default_content_set\entity_collection;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a collection implementation for files.
 *
 * This allows limiting the collection to only permanent files.
 *
 * @EntityCollection(
 *   id = "content_entity:file",
 * )
 */
class FileCollection extends ContentEntityCollection {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['conditions']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export only permament @entities', [
        '@entities' => $this->getEntityType()->getPluralLabel(),
      ]),
      '#weight' => 10,
    ];
    $configuration = $this->getConfiguration();
    if (isset($configuration['conditions']['status'])) {
      $form['conditions']['status']['#default_value'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();
    if ($form_state->getValue(['conditions', 'status'], FALSE)) {
      $configuration['conditions']['status'] = [
        'field' => 'status',
        'value' => TRUE,
        'operator' => '=',
      ];
    }
    else {
      unset($configuration['conditions']['status']);
    }
    $this->setConfiguration($configuration);
  }

}
