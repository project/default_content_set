<?php

namespace Drupal\default_content_set\Plugin\default_content_set\entity_collection;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a collection implementation for users.
 *
 * This allows limiting the collection to only active users.
 *
 * @EntityCollection(
 *   id = "content_entity:user",
 * )
 */
class UserCollection extends ContentEntityCollection {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();

    $form['conditions']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export only active @entities', [
        '@entities' => $this->getEntityType()->getPluralLabel(),
      ]),
      '#weight' => 10,
    ];
    if (isset($configuration['conditions']['status'])) {
      $form['conditions']['status']['#default_value'] = TRUE;
    }

    $form['conditions']['anonymous'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export the anonymous user'),
      '#weight' => 18,
    ];
    $form['conditions']['uuid']['#selection_settings']['include_anonymous'] = FALSE;
    if (isset($configuration['conditions']['anonymous'])) {
      $form['conditions']['anonymous']['#default_value'] = TRUE;
    }

    $form['conditions']['maintenance'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export the site maintenance account'),
      '#weight' => 19,
    ];
    if (isset($configuration['conditions']['maintenance'])) {
      $form['conditions']['maintenance']['#default_value'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();

    if ($form_state->getValue(['conditions', 'status'], FALSE)) {
      $configuration['conditions']['status'] = [
        'field' => 'status',
        'value' => TRUE,
        'operator' => '=',
      ];
    }
    else {
      unset($configuration['conditions']['status']);
    }

    if (!$form_state->getValue(['conditions', 'anonymous'], FALSE)) {
      $configuration['conditions']['anonymous'] = [
        'field' => 'uid',
        'value' => 0,
        'operator' => '<>',
      ];
    }
    else {
      unset($configuration['conditions']['anonymous']);
    }

    if (!$form_state->getValue(['conditions', 'maintenance'], FALSE)) {
      $configuration['conditions']['maintenance'] = [
        'field' => 'uid',
        'value' => 1,
        'operator' => '<>',
      ];
    }
    else {
      unset($configuration['conditions']['maintenance']);
    }

    $this->setConfiguration($configuration);
  }

}
