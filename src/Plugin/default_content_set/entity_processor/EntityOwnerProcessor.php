<?php

namespace Drupal\default_content_set\Plugin\default_content_set\entity_processor;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\default_content_set\Plugin\default_content_set\EntityProcessorBase;
use Drupal\media_entity\MediaInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an entity processor that overrides the owner of entities.
 *
 * @EntityProcessor(
 *   id = "owner",
 *   label = @Translation("Override the owner"),
 * )
 */
class EntityOwnerProcessor extends EntityProcessorBase implements PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * Constructs an entity owner processor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, EntityRepositoryInterface $entity_repository, EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityRepository = $entity_repository;
    $this->userStorage = $entity_type_manager->getStorage('user');
    $this->setStringTranslation($string_translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processEntity(EntityInterface $entity) {
    $configuration = $this->getConfiguration();
    if ($uuid = $configuration['uuid']) {
      $owner = $this->entityRepository->loadEntityByUuid('user', $uuid);

      if ($entity instanceof EntityOwnerInterface) {
        $entity->setOwner($owner);
      }
      // Support media entities from the legacy contributed Media Entity module.
      elseif ($entity instanceof MediaInterface) {
        $entity->setPublisherId($owner->id());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + ['uuid' => NULL];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $configuration = $this->getConfiguration();

    $dependencies = [];
    if ($uuid = $configuration['uuid']) {
      $dependencies['content'] = ['user:user:' . $uuid];
    }
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $form['uuid'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Owner'),
      '#target_type' => 'user',
      '#selection_settings' => [
        // The anonymous user does not have a UUID.
        'include_anonymous' => FALSE,
      ],
      '#required' => TRUE,
    ];
    if ($configuration['uuid']) {
      $user = $this->entityRepository->loadEntityByUuid('user', $configuration['uuid']);
      $form['uuid']['#default_value'] = $user;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $uid = (int) $form_state->getValue('uuid');
    // The proper value of the entity autocomplete element is only set during
    // validation, so make sure we have an actual user ID before attempting to
    // fetch the corresponding UUID.
    /* @see \Drupal\Core\Entity\Element\EntityAutocomplete::validateEntityAutocomplete() */
    if ((string) $uid === $form_state->getValue('uuid')) {
      $user = $this->userStorage->load($uid);
      $configuration['uuid'] = $user->uuid();
    }
    $this->setConfiguration($configuration);
  }

}
