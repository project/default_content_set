<?php

namespace Drupal\default_content_set\Plugin\default_content_set\entity_processor;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides an entity processor that overrides the revision owner of entities.
 *
 * @EntityProcessor(
 *   id = "revision_owner",
 *   label = @Translation("Override the revision owner"),
 * )
 */
class EntityRevisionOwnerProcessor extends EntityOwnerProcessor {

  /**
   * {@inheritdoc}
   */
  public function processEntity(EntityInterface $entity) {
    $configuration = $this->getConfiguration();
    if ($uuid = $configuration['uuid']) {
      $owner = $this->entityRepository->loadEntityByUuid('user', $uuid);

      if ($entity instanceof RevisionLogInterface) {
        $entity->setRevisionUser($owner);
      }
      elseif ($entity instanceof Paragraph) {
        $entity->setRevisionAuthorId($owner->id());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['uuid']['#title'] = $this->t('Revision owner');
    return $form;
  }

}
