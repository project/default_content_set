<?php

namespace Drupal\default_content_set\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\EntityRouteProviderInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\default_content_set\Controller\EntityCollectionController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides a route provider for default content set routes.
 */
class EntityCollectionRouteProvider implements EntityRouteProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = new RouteCollection();

    $entity_type_id = $entity_type->id();

    if ($add_page_route = $this->getCollectionAddPageRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.entity_collection_add_page", $add_page_route);
    }

    if ($add_form_route = $this->getCollectionAddFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.entity_collection_add_form", $add_form_route);
    }

    if ($edit_route = $this->getCollectionEditFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.entity_collection_edit_form", $edit_route);
    }

    if ($delete_route = $this->getCollectionDeleteFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.entity_collection_delete_form", $delete_route);
    }

    return $collection;
  }

  /**
   * Gets the add-page route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCollectionAddPageRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('entity-collection-add-page')) {
      $route = new Route($entity_type->getLinkTemplate('entity-collection-add-page'));

      $title = new TranslatableMarkup('Add content collection');

      $route
        ->setDefault('_title', $title->getUntranslatedString())
        ->setDefault('_controller', EntityCollectionController::class . '::addPage')
        ->setDefault('_title', 'Add content collection')
        ->setRequirement('_entity_access', 'default_content_set.update')
        ->setOption('parameters', [
          $entity_type->id() => ['type' => 'entity:' . $entity_type->id()],
        ]);

      return $route;
    }
  }

  /**
   * Gets the add-form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCollectionAddFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('entity-collection-add-form') && ($form_class = $entity_type->getFormClass('add-entity-collection'))) {
      $route = new Route($entity_type->getLinkTemplate('entity-collection-add-form'));

      $route
        ->setDefault('_title_callback', $form_class . '::addTitle')
        ->setDefault('_form', $form_class)
        ->setRequirement('_entity_access', 'default_content_set.update')
        /* @see \Drupal\Core\Render\Element\MachineName::processMachineName() */
        // Allow for colons to support plugin IDs of derivatives.
        ->setRequirement('entity_collection_plugin', '[a-z0-9_:]+')
        ->setOption('parameters', [
          $entity_type->id() => ['type' => 'entity:' . $entity_type->id()],
        ]);

      return $route;
    }
  }

  /**
   * Gets the edit-form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCollectionEditFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('entity-collection-edit-form') && ($form_class = $entity_type->getFormClass('edit-entity-collection'))) {
      $route = new Route($entity_type->getLinkTemplate('entity-collection-edit-form'));

      $route
        ->setDefault('_title_callback', $form_class . '::editTitle')
        ->setDefault('_form', $form_class)
        ->setRequirement('_entity_access', 'default_content_set.update')
        ->setOption('parameters', [
          $entity_type->id() => ['type' => 'entity:' . $entity_type->id()],
        ])
        /* @see \Drupal\Core\Render\Element\MachineName::processMachineName() */
        ->setRequirement('entity_collection', '[a-z0-9_]+');

      return $route;
    }
  }

  /**
   * Gets the delete-form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCollectionDeleteFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('entity-collection-delete-form') && ($form_class = $entity_type->getFormClass('delete-entity-collection'))) {
      $route = new Route($entity_type->getLinkTemplate('entity-collection-delete-form'));

      $route
        ->setDefault('_title_callback', $form_class . '::deleteTitle')
        ->setDefault('_form', $form_class)
        ->setRequirement('_entity_access', 'default_content_set.update')
        ->setOption('parameters', [
          $entity_type->id() => ['type' => 'entity:' . $entity_type->id()],
        ])
        /* @see \Drupal\Core\Render\Element\MachineName::processMachineName() */
        ->setRequirement('entity_collection', '[a-z0-9_]+');

      return $route;
    }
  }

}
